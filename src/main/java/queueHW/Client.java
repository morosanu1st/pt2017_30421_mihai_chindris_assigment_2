package queueHW;

public class Client {
	private int waitTime=0;
	private int arrTime;
	private int servTime;
	private boolean served;
	private int finishTime;
	private Client next;
	
	Client(int arr,int serv)
	{
		arrTime=arr;
		servTime=serv;
		next=null;
		served=false;
	}
	
	public int getArrTime(){
		return arrTime;
	}
	public int getFinishTime(){
		return finishTime;
	}
	
	public void secPass(){
		if(!served){
			waitTime++;
			}
		else
			servTime--;
	}
	
	public void leave(int time){
		finishTime=time;
	}
	
	public int getWaitTime(){
		return waitTime;
	}

	public int getServTime() {
		return servTime;
	}


	public Client getNext() {
		return next;
	}

	public void setNext(Client next) {
		this.next = next;
	}

	public boolean isServed() {
		return served;
	}

	public void setServed(boolean served) {
		this.served = served;
	}

	

}
