package queueHW;

public class Timer extends Thread {
	private int time=0;
	private int duration;
	
	Timer(int t)
	{
		duration=t;
	}
	public void run(){
		
		while(time<duration){
			long updateTime=System.currentTimeMillis();
			
			while(System.currentTimeMillis()-updateTime<1000)
				;
			time++;
			
		}
	}
	 public int getTime(){
		 return time;
	 }
}
