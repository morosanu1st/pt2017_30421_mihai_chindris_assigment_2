package queueHW;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Gui {
	JLabel error;
	SimulationWindow s;
	int running=0;
	private JFrame frmQueueSimulation;
	private JTextField textTime;
	private JTextField textMinGap;
	private JTextField textMaxGap;
	private JTextField textMinServ;
	private JTextField textQueues;
	private JTextField textMaxServ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					window.frmQueueSimulation.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmQueueSimulation = new JFrame();
		frmQueueSimulation.setTitle("Simulation Setup");
		frmQueueSimulation.setBounds(100, 100, 450, 300);
		frmQueueSimulation.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmQueueSimulation.getContentPane().setLayout(null);
		
		textTime = new JTextField();
		textTime.setText("15");
		textTime.setToolTipText("Type the simulation time");
		textTime.setBounds(10, 231, 55, 20);
		frmQueueSimulation.getContentPane().add(textTime);
		textTime.setColumns(10);
		
		textMinGap = new JTextField();
		textMinGap.setText("1");
		textMinGap.setToolTipText("Type the minimum gap between the arrival of 2 customers");
		textMinGap.setBounds(139, 231, 55, 20);
		frmQueueSimulation.getContentPane().add(textMinGap);
		textMinGap.setColumns(10);
		
		textMaxGap = new JTextField();
		textMaxGap.setText("2");
		textMaxGap.setToolTipText("Type the maximum gap between the arrival of 2 customers");
		textMaxGap.setBounds(204, 231, 55, 20);
		frmQueueSimulation.getContentPane().add(textMaxGap);
		textMaxGap.setColumns(10);
		
		textMinServ = new JTextField();
		textMinServ.setText("5");
		textMinServ.setToolTipText("Type the minimum service time");
		textMinServ.setBounds(269, 231, 55, 20);
		frmQueueSimulation.getContentPane().add(textMinServ);
		textMinServ.setColumns(10);
		
		textQueues = new JTextField();
		textQueues.setText("4");
		textQueues.setToolTipText("Type the number of queueus");
		textQueues.setBounds(74, 231, 55, 20);
		frmQueueSimulation.getContentPane().add(textQueues);
		textQueues.setColumns(10);
		
		textMaxServ = new JTextField();
		textMaxServ.setText("6");
		textMaxServ.setToolTipText("Type the maximum service time");
		textMaxServ.setBounds(334, 231, 55, 20);
		frmQueueSimulation.getContentPane().add(textMaxServ);
		textMaxServ.setColumns(10);
		
		error=new JLabel();
		error.setBounds(25, 130, 400, 23);
		frmQueueSimulation.getContentPane().add(error);
		
		JLabel lblNewLabel = new JLabel("Time");
		lblNewLabel.setBounds(10, 206, 46, 14);
		frmQueueSimulation.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Queues");
		lblNewLabel_1.setBounds(74, 206, 46, 14);
		frmQueueSimulation.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("MinGap");
		lblNewLabel_2.setBounds(137, 206, 46, 14);
		frmQueueSimulation.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("MaxGap");
		lblNewLabel_3.setBounds(204, 206, 46, 14);
		frmQueueSimulation.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("MinServ");
		lblNewLabel_4.setToolTipText("Type the minimum service time");
		lblNewLabel_4.setBounds(269, 206, 46, 14);
		frmQueueSimulation.getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("MaxServ");
		lblNewLabel_5.setToolTipText("Type the maximum service time");
		lblNewLabel_5.setBounds(334, 206, 46, 14);
		frmQueueSimulation.getContentPane().add(lblNewLabel_5);
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			int i=0;
			
			public void actionPerformed(ActionEvent arg0) {
				
				if(i==0){
				i=1;
				int time=0;
				int nrOfQueues=0;
				int minGap=0;
				int maxGap=0;
				int minServ=0;
				int maxServ=0;
					try{
				 time=Integer.parseInt(textTime.getText());
				 nrOfQueues=Integer.parseInt(textQueues.getText());
				 minGap=Integer.parseInt(textMinGap.getText());
				 maxGap=Integer.parseInt(textMaxGap.getText());
				 minServ=Integer.parseInt(textMinServ.getText());
				 maxServ=Integer.parseInt(textMaxServ.getText());
				
				}
					catch(NumberFormatException e){
						error.setText(" ");
						error.setText("please make sure you entered only positive integers in those fields");
						i=0;
					}
					if(i==1){
						if(nrOfQueues>0){
						error.setText(" ");
						Manager manager=new Manager(time+1,nrOfQueues,minGap,maxGap,minServ,maxServ);
						manager.start();
						s=new SimulationWindow(manager);
						s.makeVisible();
						s.start();
						frmQueueSimulation.setVisible(false);}
						else{
							error.setText("Please insert a valid number of queues");
							i=1;
							i=0;
							}
					}
				}
					
					
			}
		});
		btnStart.setBounds(139, 91, 89, 23);
		frmQueueSimulation.getContentPane().add(btnStart);
	}
}
